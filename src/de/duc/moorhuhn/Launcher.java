package de.duc.moorhuhn;

import de.duc.moorhuhn.core.Game;

/**
 * Created by Corvin on 31.10.2016.
 */
public class Launcher {

    /*
        +-----------------------------------------------------+
        |               T   O   D   O                         |
        +-----------------------------------------------------+

        => Implement Weapon images
     */

    public static void main(String[] args) {
        Game game = new Game(1920, 1080, true);
        game.start();
    }

}
