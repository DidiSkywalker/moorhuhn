package de.duc.moorhuhn.core;

import de.duc.moorhuhn.core.states.GameState;
import de.duc.moorhuhn.core.states.MenuState;
import de.duc.moorhuhn.core.states.StateManager;
import de.duc.moorhuhn.gfx.Asset;
import de.duc.moorhuhn.input.KeyManager;
import de.duc.moorhuhn.input.MouseManager;
import de.duc.moorhuhn.lang.MessageManager;
import de.duc.moorhuhn.sound.SoundPlayer;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;

/**
 * Created by Corvin on 31.10.2016.
 */
public class Game implements Runnable {

    private StateManager stateManager;
    private MessageManager messageManager;

    // Frame stuff
    private JFrame frame;
    private Canvas canvas;
    private BufferStrategy bs;
    private Graphics g;

    // Frame size
    private int width, height;
    private boolean fullscreen;

    // Listeners
    private KeyManager keyManager;
    private MouseManager mouseManager;

    // Thread
    private Thread thread;
    private boolean running;
    private static boolean devMode = false;

    private int ticks;

    public Game(int width, int height, boolean fullscreen) {
        new Asset();
        this.width = width;
        this.height = height;
        this.fullscreen = fullscreen;

        stateManager = new StateManager();
        messageManager = new MessageManager("en");
        stateManager.addState(new MenuState("menu", this));
        stateManager.addState(new GameState("game", this));

        initFrame();
    }

    private void initFrame() {
        // Set up the frame
        frame = new JFrame("Moorhuhn!");
        keyManager = new KeyManager(this);
        mouseManager = new MouseManager(this);

        frame.setTitle("Moorhuhn!");
        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if(fullscreen) {
            frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            frame.setUndecorated(true);
        }
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setAutoRequestFocus(true);
        frame.setVisible(true);
        frame.addKeyListener(keyManager);

        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(width, height));
        canvas.setMinimumSize(new Dimension(width, height));
        canvas.setMaximumSize(new Dimension(width, height));
        canvas.addMouseListener(mouseManager);
        canvas.addMouseMotionListener(mouseManager);
        canvas.setFocusable(false);

        frame.add(canvas);
        frame.pack();
    }

    private void update() {
        stateManager.getState().update();
    }

    private void draw() {
        bs = canvas.getBufferStrategy();
        if(bs == null) {
            canvas.createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();
        // Clear
        g.clearRect(0, 0, width, height);

        // Draw
        stateManager.getState().draw(g);

        // Finish
        bs.show();
        g.dispose();
    }

    @Override
    public void run() {
        int fps = 60;
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        ticks = 0;

        while(running) {
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;

            if(delta >= 1) {
                update();
                draw();

                ticks++;
                delta--;
            }

            if(timer >= 1000000000) {
                System.out.println("Ticks and Frames per second: "+ticks);
                ticks = 0;
                timer = 0;
            }
        }
    }

    public synchronized void start() {
        if(running) return;

        stateManager.setState("menu");
        stateManager.getState().init();
        running = true;
        thread = new Thread(this);
        thread.start();

        SoundPlayer.playSound(Asset.theme, 30);
    }

    public synchronized void stop() {
        if(!running) return;

        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startGame() {
        stateManager.setState("game");
        stateManager.getState().init();
    }

    // GETTER
    public boolean isInGame() { return stateManager.getState().equals(stateManager.getState("game")); }
    public int getWidth() { return width; }
    public int getHeight() { return height; }
    public JFrame getFrame() { return frame; }
    public StateManager getStateManager() { return stateManager; }
    public MouseManager getMouseManager() { return mouseManager; }
    public KeyManager getKeyManager() { return keyManager; }
    public MessageManager getMessageManager() { return messageManager; }

    // SETTER ?

    // STATIC
    public static boolean isDevMode() { return devMode; }
    public static void setDevMode(boolean b) { devMode = b; }

}
