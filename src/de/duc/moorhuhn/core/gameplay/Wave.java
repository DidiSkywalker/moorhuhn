package de.duc.moorhuhn.core.gameplay;

import de.duc.moorhuhn.core.states.GameState;
import de.duc.moorhuhn.entities.gameentities.powerups.HealthPU;
import de.duc.moorhuhn.entities.gameentities.powerups.PowerUp;

/**
 * Created by Corvin on 01.11.2016.
 */
public class Wave {

    private GameState gameState;

    private int level;

    private int huehner;
    private double speed;
    private double spawnDelay;

    private double doubleSpawnChance;

    private int ticks;
    private int seconds;
    private int huehnerSpawned;

    public Wave(GameState gameState) {
        this.gameState = gameState;

        level = 1;

        huehner = 15;
        speed = 4;
        spawnDelay = 4;

        doubleSpawnChance = 0.0;

        seconds = 0;
        huehnerSpawned = 0;
    }

    public void update() {
        if(!gameState.isGameOver()) {
            ticks++;
            if(ticks >= 20) {
                seconds++;
                if(seconds >= spawnDelay) {
                    if(huehnerSpawned < huehner) {
                        gameState.spawnHuhn((int)speed);  // First spawn
                        if(doubleSpawnChance/100 >= Math.random()) {
                            System.out.println("Doubles spawned!");
                            gameState.spawnHuhn((int)speed);   // If doubleSpawn, spawn a second chicken
                        }
                        huehnerSpawned++;
                        //System.out.println("Huhn gespawnt. Hühner: "+huehnerSpawned);
                    } else {
                        // Power Ups
                        PowerUp powerUp = new HealthPU(gameState, 0, 0);
                        gameState.spawnPowerUp(powerUp);
                        nextWave();
                        System.out.println("Next wave: "+level);
                    }
                    seconds = 0;
                }
                ticks = 0;
            }
        }
    }

    public void nextWave() {
        huehnerSpawned = 0;

        level++;
        huehner += level;
        speed += 0.5*level;
        spawnDelay -= 0.01;

        doubleSpawnChance = level;
    }

    public int getLevel() { return level; }
}
