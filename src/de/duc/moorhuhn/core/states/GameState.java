package de.duc.moorhuhn.core.states;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.core.gameplay.Wave;
import de.duc.moorhuhn.entities.Entity;
import de.duc.moorhuhn.entities.gameentities.GameEntity;
import de.duc.moorhuhn.entities.gameentities.Moorhuhn;
import de.duc.moorhuhn.entities.gameentities.powerups.PowerUp;
import de.duc.moorhuhn.gfx.Asset;
import de.duc.moorhuhn.util.Debug;
import de.duc.moorhuhn.weapons.Shotgun;
import de.duc.moorhuhn.weapons.Weapon;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Corvin on 31.10.2016.
 */
public class GameState extends State {

    private boolean gameOver;                       // true when player lost all lives
    private Wave wave;                              // WaveManager

    private Set<GameEntity> entities;               // currently active entities
    private Set<GameEntity> entitiesToRemove;       // entities to be removed next tick to avoid ConcurrentModificationException

    // GAMEPLAY SETTINGS
    private final int HEALTH_CAP;
    private int health, points, kills;
    public int shotsFired;
    private long startTime;                         // Starttime of the round, used to calculate time survived at gameover

    private Weapon weapon;                          // Currently used weapon

    // Misc
    private Random random;                          // Used for spawning entities
    // Animation
    private boolean animateDamage;                  // true when broken heart should be displayed
    private long animateDamageStart;                // time damage animation started, used to calculate when to end

    public GameState(String name, Game game) {
        super(name, game);

        entities = new HashSet<>();
        entitiesToRemove = new HashSet<>();

        HEALTH_CAP = 30;
    }

    public void addEntity(GameEntity entity) { entities.add(entity);
        Debug.send("Entities: "+entities.size()); }
    public void removeEntity(Entity entity) { entities.remove(entity);
        Debug.send("Entities: "+entities.size()); }
    public void killEntity(GameEntity entity) { entitiesToRemove.add(entity); }
    public void addPoints(int points) { this.points += points; }
    public void scoreKill() { kills++; }
    public void heal(int amount) {
        if(this.health+amount < HEALTH_CAP) {
            this.health += amount;
        } else {
            this.health += (HEALTH_CAP-this.health);
        }
    }
    public void damage() {
        if(health > 0) {
            health--;
            animateDamage = true;
            animateDamageStart = System.currentTimeMillis();
        }

        if(health <= 0) gameOver();
    }

    private void gameOver() {
        gameOver = true;
    }

    @Override
    public void update() {
        if(!active) return;
        entitiesToRemove.forEach(this::removeEntity);
        entitiesToRemove.clear();
        entities.forEach(GameEntity::update);

        if(wave == null) {
            wave = new Wave(this);
            return;
        }
        wave.update();
        if(!gameOver && weapon.isAutomatic() && getMouseManager().isMousePressed()) leftClick(getMouseManager().getMouseLocation());
    }

    @Override
    public void draw(Graphics g) {
        if(!active) return;
        // Background
        g.drawImage(Asset.background, 0, 0, null);

        // Draw Entities
        for(Entity entity : entities) entity.draw(g);

        // Draw health
        if(animateDamage) { // Chech if broken heart should be drawn
            if(System.currentTimeMillis() >= animateDamageStart+300)
                animateDamage = false;
        }
        drawHealth(g, (animateDamage || gameOver) ? Asset.heart_damaged:Asset.heart);

        // Draw weapon

        // Draw Overlay
        g.drawImage(Asset.background_overlay, 0, getHeight()-(getHeight()/4)+48, null);

        // Draw labels
        g.setColor(Color.black);
        g.setFont(new Font("Monospaced", Font.BOLD, 20));
        g.drawString(getMessageManager().get("points")+": "+points, 10, 20);

        g.drawString(getMessageManager().get("shots")+": "+shotsFired, 10, getHeight()-10);
        g.drawString(getMessageManager().get("kills")+": "+kills, 10, getHeight()-40);
        g.drawString(getMessageManager().get("wave")+": "+wave.getLevel(), 10, getHeight()-70);

        // Draw gun magazine
        drawGunMag(g);

        // Draw GameOver
        if(gameOver) {
            g.setColor(Color.red);
            g.setFont(new Font("Monospaced", Font.BOLD, 180));
            g.drawString(getMessageManager().get("gameover"), (getWidth()/2)-500, (getHeight()/2)-90);
        }

        // Draw crosshair
        Point mouseLocation = getMouseManager().getMouseLocation();
        g.drawImage(Asset.crosshair, mouseLocation.x-Asset.crosshair.getWidth()/2, mouseLocation.y-Asset.crosshair.getHeight()/2, null);
    }

    @Override
    public void leftClick(Point p) {
        if(!active) return;
        shotsFired += weapon.fire(p, getEntityAt(p));
    }

    @Override
    public void rightClick(Point p) {
    }

    @Override
    public void init() {
        active = true;
        gameOver = false;
        health = 20;
        points = 0;
        kills = 0;
        shotsFired = 0;
        startTime = 0;

        wave = new Wave(this);
        weapon = new Shotgun();
        hideCursor();
    }

    // DRAWING METHODS
    private void drawHealth(Graphics g, BufferedImage image) {
        g.drawImage(image, getWidth()-160, 15, null);
        g.setColor(Color.red);
        g.setFont(new Font("Monospaced", Font.BOLD, 40));
        g.drawString(""+health, getWidth()-80, 70);
    }
    private void drawGunMag(Graphics g) {
        int width = weapon.getAmmoIcon().getWidth();
        for(int i = 0; i < weapon.getCurrentMag(); i++) {
            g.drawImage(weapon.getAmmoIcon(), (getWidth()-35)-i*(width+5), (getHeight()-5)-weapon.getAmmoIcon().getHeight(), null);
        }
    }

    private void hideCursor() {
        // Mouse-Cursor verstecken (Credit: coobird (stackoverflow))
        // Transparent 16 x 16 pixel cursor image.
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TRANSLUCENT);

        // Create a new blank cursor.
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                cursorImg, new Point(0, 0), "blank cursor");

        // Set the blank cursor to the JFrame.
        game.getFrame().getContentPane().setCursor(blankCursor);
    }

    public void spawnHuhn(int speed) {
        random = new Random();
        int y = random.nextInt((getHeight()/2)-70);
        if(y < 50) y = 50;
        Moorhuhn moorhuhn = new Moorhuhn(this, 0, y, speed);
        addEntity(moorhuhn);
    }

    public void spawnPowerUp(PowerUp powerUp) {
        random = new Random();
        int x = random.nextInt(getWidth());
        if(x < 140) x = 140;
        if(x > getWidth()-140) x = getWidth()-140;
        powerUp.setX(x);
        addEntity(powerUp);
        powerUp.spawn();
    }

    public GameEntity getEntityAt(Point point) {
        for(GameEntity entity : entities) {
            if(point.getY() >= entity.getHitboxY() && point.getY() <= entity.getHitboxHeight()) {
                if(point.getX() >= entity.getHitboxX() && point.getX() <= entity.getHitboxWidth()) {
                    return entity;
                }
            }
        }
        return null;
    }

    public Weapon getWeapon() { return weapon; }
    public boolean isGameOver() { return gameOver; }
}
