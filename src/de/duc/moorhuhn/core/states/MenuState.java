package de.duc.moorhuhn.core.states;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.entities.menucomponents.MenuComponent;
import de.duc.moorhuhn.entities.menucomponents.Title;
import de.duc.moorhuhn.entities.menucomponents.button.Button;
import de.duc.moorhuhn.entities.menucomponents.button.ButtonListener;
import de.duc.moorhuhn.entities.menucomponents.checkbox.CheckBox;
import de.duc.moorhuhn.entities.menucomponents.checkbox.CheckBoxGroup;
import de.duc.moorhuhn.entities.menucomponents.checkbox.CheckBoxListener;
import de.duc.moorhuhn.gfx.Asset;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Corvin on 31.10.2016.
 */
public class MenuState extends State {

    private Title title;
    private Button startButton, settingsButton, exitButton, backButton;
    private CheckBox cbDE, cbEN, cbDev;
    private boolean init;

    private Set<MenuComponent> menuComponents;
    private Set<MenuComponent> settingsComponents;

    private boolean settings;
    private long toggleTime;

    private MenuComponent currentlySelected; // Component the mouse is currently in
    private MenuComponent lastSelected; // Last selected component

    public MenuState(String name, Game game) {
        super(name, game);
        init = false;
        menuComponents = new HashSet<>();
        settingsComponents = new HashSet<>();
        settings = false;
        toggleTime = 0;
        initComponents();
    }

    @Override
    public void update() {
        if(!active) return;
        selectComponent();

        // Components
        if(settings) for(MenuComponent component : settingsComponents) component.update();
        else for(MenuComponent component : menuComponents) component.update();
    }

    @Override
    public void draw(Graphics g) {
        if(!active) return;
        // Background
        g.drawImage(Asset.background, 0, 0, null);

        // Components
        if(settings) for(MenuComponent component : settingsComponents) component.draw(g);
        else for(MenuComponent component : menuComponents) component.draw(g);
    }

    @Override
    public void leftClick(Point p) {
        if(!active) return;
        MenuComponent component = getComponentAt(p);
        if(component != null) component.hit(p);
    }

    @Override
    public void rightClick(Point p) {
    }

    @Override
    public void init() {
        active = true;
        // Custom cursor setzen
        Cursor customCursor = Toolkit.getDefaultToolkit().createCustomCursor(
                Asset.cursor, new Point(0, 0), "custom cursor");
        game.getFrame().getContentPane().setCursor(customCursor);
    }

    private void selectComponent() {
        MenuComponent component = getComponentAt(getMouseManager().getMouseLocation());
        if(component != null) {
            if(lastSelected == null || !component.equals(currentlySelected)) {
                // lastSelected is null and current component is not the currentlySelected -> entering new Component
                component.mouseEnter();
                currentlySelected = component;
                lastSelected = component;
            }
        } else {
            if(lastSelected != null) {
                lastSelected.mouseLeave();
                currentlySelected = null;
                lastSelected = null;
            }
        }
    }

    public MenuComponent getComponentAt(Point point) {
        Set<MenuComponent> set = new HashSet<>();
        set = settings ? settingsComponents:menuComponents;
        for(MenuComponent entity : set) {
            if(point.getY() >= entity.getHitboxY() && point.getY() <= entity.getHitboxHeight()) {
                if(point.getX() >= entity.getHitboxX() && point.getX() <= entity.getHitboxWidth()) {
                    return entity;
                }
            }
        }
        return null;
    }

    private void initComponents() {

        title = new Title("Moorhuhn!", getWidth()/2, getHeight()/2-300);
        menuComponents.add(title);
        settingsComponents.add(title);

        startButton = new Button(getWidth()/2, (getHeight()/2)-100, null);
        startButton.setText(getMessageManager().get("startButton"));
        startButton.addButtonListener(new ButtonListener() {
            @Override
            public void click() {
                // Start Game
                game.startGame();
            }
        });
        menuComponents.add(startButton);

        settingsButton = new Button(getWidth()/2, startButton.getY()+170, null);
        settingsButton.setText(getMessageManager().get("settingsButton"));
        settingsButton.addButtonListener(new ButtonListener() {
            @Override
            public void click() {
                settings = true;
                currentlySelected.mouseLeave();
            }
        });
        menuComponents.add(settingsButton);

        exitButton = new Button(getWidth()/2, settingsButton.getY()+170, null);
        exitButton.setText(getMessageManager().get("exitButton"));
        exitButton.addButtonListener(new ButtonListener() {
            @Override
            public void click() {
                if(toggleTime+5 <= System.currentTimeMillis())
                    System.exit(0);
            }
        });
        menuComponents.add(exitButton);

        backButton = new Button(getWidth()/2, settingsButton.getY()+170, null);
        backButton.setText(getMessageManager().get("backButton"));
        backButton.addButtonListener(new ButtonListener() {
            @Override
            public void click() {
                settings = false;
                toggleTime = System.currentTimeMillis();
                // TODO: save settings done?
            }
        });
        settingsComponents.add(backButton);

        cbDev = new CheckBox(getWidth()/2, startButton.getY()+170, null);
        cbDev.setText(getMessageManager().get("devMode"));
        settingsComponents.add(cbDev);

        CheckBoxListener listener = new CheckBoxListener() {

            @Override
            public void toggle(CheckBox box, boolean b) {
                getMessageManager().setLang(box.getId());
                updateNames();
            }

        };
        CheckBoxGroup langGroup = new CheckBoxGroup();

        cbDE = new CheckBox(getWidth()/2-70, (getHeight()/2)-100, Asset.de);
        cbDE.setId("de");
        cbDE.setChecked(true);

        cbEN = new CheckBox(getWidth()/2+70, cbDE.getY(), Asset.en);
        cbEN.setId("en");

        cbDE.addListener(listener);
        cbEN.addListener(listener);

        settingsComponents.add(cbDE);
        settingsComponents.add(cbEN);

        langGroup.addBox(cbDE);
        langGroup.addBox(cbEN);
        langGroup.setCurrentlyChecked(cbDE);

        init = true;
    }

    private void updateNames() {
        if(!init) return;
        startButton.setText(getMessageManager().get("startButton"));
        settingsButton.setText(getMessageManager().get("settingsButton"));
        exitButton.setText(getMessageManager().get("exitButton"));
        backButton.setText(getMessageManager().get("backButton"));
        cbDev.setText(getMessageManager().get("devMode"));
    }

}
