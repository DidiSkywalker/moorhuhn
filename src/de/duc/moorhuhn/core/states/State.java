package de.duc.moorhuhn.core.states;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.input.KeyManager;
import de.duc.moorhuhn.input.MouseManager;
import de.duc.moorhuhn.lang.MessageManager;

import java.awt.*;

/**
 * Created by Corvin on 31.10.2016.
 */
public abstract class State {

    protected String name;
    protected Game game;
    protected boolean active;

    public State(String name, Game game) {
        this.name = name;
        this.game = game;
    }

    public abstract void update();
    public abstract void draw(Graphics g);
    public abstract void leftClick(Point p);
    public abstract void rightClick(Point p);
    public abstract void init();
    public void notify(String msg) {
        if(Game.isDevMode()) System.out.println("["+name+"]: "+msg);
    }

    public Game getGame() { return game; }
    public String getName() { return name; }

    public void setActive(boolean b) { active = b; }

    public int getWidth() { return game.getWidth(); }
    public int getHeight() { return game.getHeight(); }
    protected MouseManager getMouseManager() { return game.getMouseManager(); }
    protected KeyManager getKeyManager() { return game.getKeyManager(); }
    protected StateManager getStateManager() { return game.getStateManager(); }
    protected MessageManager getMessageManager() { return game.getMessageManager(); }
}
