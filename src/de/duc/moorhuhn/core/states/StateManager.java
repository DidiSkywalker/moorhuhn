package de.duc.moorhuhn.core.states;

import com.sun.istack.internal.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Corvin on 31.10.2016.
 */
public class StateManager {

    private State currentState;
    private Map<String, State> states;

    public StateManager() {
        states = new HashMap<>();
    }

    @Nullable
    public State getState() { return currentState; }
    @Nullable
    public State getState(String name) { return states.get(name); }

    public void addState(State state) {
        states.put(state.getName(), state);
        state.notify("registered in StateManager");
    }
    public void setState(State state) { currentState = state; }
    public void setState(String name) { currentState = getState(name); }


}
