package de.duc.moorhuhn.entities;

import java.awt.*;

/**
 * Created by Corvin on 31.10.2016.
 */
public abstract class Entity {

    protected int x, y;
    protected final EntityType type;
    protected boolean enabled;

    // Hitbox
    protected int width, height;

    public Entity(EntityType type) {
        this.type = type;
    }

    public Entity(EntityType type,  int x, int y) {
        this.type = type;
        this.x = x;
        this.y = y;
    }

    public Entity(EntityType type,  int x, int y, int width, int height) {
        this.type = type;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() { return x; }
    public int getY() { return y; }
    public int getWidth() { return width; }
    public int getHeight() { return height; }
    public EntityType getType() { return type; }
    public boolean isEnabled() { return enabled; }

    public void setX(int x) { this.x = x; }
    public void setY(int y) { this.y = y; }
    public void setWidth(int width) { this.width = width; }
    public void setHeight(int height) { this.height = height; }
    public void setEnabled(boolean enabled) { this.enabled = enabled; }

    public abstract void update();
    public abstract void draw(Graphics g);
    public abstract void hit(Point p);


}
