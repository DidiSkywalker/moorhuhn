package de.duc.moorhuhn.entities;

/**
 * Created by Corvin on 31.10.2016.
 */
public enum EntityType {

    // GameEntities
    MOORHUHN,
    POWERUP,
    ZEPELIN,
    AIRBALLOON,

    // MenuComponents
    BUTTON,
    TITLE,
    CHECKBOX


}
