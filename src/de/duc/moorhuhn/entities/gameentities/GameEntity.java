package de.duc.moorhuhn.entities.gameentities;

import de.duc.moorhuhn.core.states.GameState;
import de.duc.moorhuhn.entities.Entity;
import de.duc.moorhuhn.entities.EntityType;

/**
 * Created by Corvin on 31.10.2016.
 */
public abstract class GameEntity extends Entity {

    protected GameState gameState;
    protected int speed;
    protected int hitboxX, hitboxY, hitboxW, hitboxH;

    public GameEntity(GameState gameState, EntityType type) {
        super(type);
        this.gameState = gameState;
    }
    public GameEntity(GameState gameState, EntityType type, int x, int y) {
        super(type, x, y);
        this.gameState = gameState;
    }
    public GameEntity(GameState gameState, EntityType type, int x, int y, int width, int height) {
        super(type, x, y, width, height);
        this.gameState = gameState;
    }
    public GameEntity(GameState gameState, EntityType type, int x, int y, int width, int height, int speed) {
        super(type, x, y, width, height);
        this.gameState = gameState;
        this.speed = speed;
    }

    public int getSpeed() { return speed; }
    public void setSpeed(int speed) { this.speed = speed; }

    public int getHitboxX() { return hitboxX; }
    public int getHitboxY() { return hitboxY; }
    public int getHitboxWidth() { return hitboxX+hitboxW; }
    public int getHitboxHeight() { return hitboxY+hitboxH; }

    public void setHitBox(int x, int y, int width, int height) {
        hitboxX = x;
        hitboxY = y;
        hitboxW = width;
        hitboxH = height;
    }

    public abstract void kill();
    public void remove() {
        gameState.killEntity(this);
        enabled = false;
    }

}
