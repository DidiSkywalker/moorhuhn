package de.duc.moorhuhn.entities.gameentities;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.core.states.GameState;
import de.duc.moorhuhn.entities.EntityType;
import de.duc.moorhuhn.gfx.Asset;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Corvin on 01.11.2016.
 */
public class Moorhuhn extends GameEntity {

    private static BufferedImage[] animation;
    private int animationStage;
    private boolean forward;

    private boolean dead;
    private boolean drawMarker;
    private int markerTicks = 7;
    private Point hitPoint;

    private int dy; // delta y

    private int ticks;

    static {
        animation = new BufferedImage[3];

        animation[0] = Asset.moorhuhn_1;
        animation[1] = Asset.moorhuhn_2;
        animation[2] = Asset.moorhuhn_3;
    }


    public Moorhuhn(GameState gameState, int x, int y, int speed) {
        super(gameState, EntityType.MOORHUHN, x, y, Asset.moorhuhn_1.getWidth(), Asset.moorhuhn_1.getHeight());
        updateHitbox();
        this.speed = speed;
        animationStage = 0;
        forward = true;
        ticks = 0;
        dead = false;
        enabled = true;
        dy = 0;
    }

    @Override
    public void kill() {
        drawMarker = true;
        gameState.scoreKill();
        gameState.addPoints(25);
        dead = true;
    }

    @Override
    public void update() {
        if(enabled) {
            move();
            updateHitbox();
        }
    }

    @Override
    public void draw(Graphics g) {
        if(!dead) {
            g.drawImage(animation[animationStage], x, y+dy, null);

            if(Game.isDevMode()) {
                g.setColor(Color.yellow);
                g.drawRect(hitboxX, hitboxY, hitboxW, hitboxH);
            }
        } else {
            if(drawMarker) {
                g.drawImage(Asset.hitmarker, (int)hitPoint.getX()-16, (int)hitPoint.getY()-15, null);
                markerTicks--;
                if(markerTicks <= 0) drawMarker = false;
            }
            g.drawImage(Asset.moorhuhn_dead, x, y, null);
        }
    }

    @Override
    public void hit(Point p) {
        this.hitPoint = p;
        if(!dead) kill();
    }

    private void move() {
        if(!dead) {
            // Move
            x += speed;
            dy = (int)(70*Math.sin(x*0.01));

            if(x >= gameState.getWidth()) {
                // Destroy
                gameState.damage();
                remove();
            }

            // Animate
            if(ticks >= 10) {
                animationStage += forward ? 1:-1;
                if(animationStage == 3) {
                    animationStage = 2;
                    forward = false;
                }
                if(animationStage == 0 && !forward) {
                    forward = true;
                }

                ticks = 0;
            }
            ticks++;
        } else {
            y += 10;
            if(y >= gameState.getHeight()) remove();
        }
    }

    private void updateHitbox() {
        setHitBox(x, y+height/3+dy, width, height/3);
    }
}
