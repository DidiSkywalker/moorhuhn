package de.duc.moorhuhn.entities.gameentities.powerups;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.core.states.GameState;
import de.duc.moorhuhn.gfx.Asset;

import java.awt.*;

/**
 * Created by Corvin on 01.11.2016.
 */
public class HealthPU extends PowerUp {

    public HealthPU(GameState gameState, int x, int speed) {
        super(gameState, x, speed, Asset.lootCrate_health);
        this.width = image.getWidth();
        this.height = image.getHeight();
        updateHitbox();
    }

    @Override
    public void spawn() {
        enabled = true;
    }

    @Override
    public void kill() {

    }

    @Override
    public void update() {
        if(enabled) {
            move(0.5, 0.2);
            updateHitbox();
        }
    }

    @Override
    public void draw(Graphics g) {
        if(enabled) {
            g.drawImage(image, x, y, null);
            if(Game.isDevMode()) {
                g.setColor(Color.yellow);
                g.drawRect(hitboxX, hitboxY, hitboxW, hitboxH);
            }
        }
    }

    @Override
    public void hit(Point p) {
        System.out.println("Health Powerup getroffen!");
        gameState.heal(5);
        remove();
    }

    private void updateHitbox() {
        setHitBox(x, y+height/2, width, height/2);
    }
}
