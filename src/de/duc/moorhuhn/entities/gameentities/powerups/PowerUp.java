package de.duc.moorhuhn.entities.gameentities.powerups;

import de.duc.moorhuhn.core.states.GameState;
import de.duc.moorhuhn.entities.EntityType;
import de.duc.moorhuhn.entities.gameentities.GameEntity;

import java.awt.image.BufferedImage;

/**
 * Created by Corvin on 01.11.2016.
 */
public abstract class PowerUp extends GameEntity {

    protected BufferedImage image;
    protected float speed;

    public PowerUp(GameState gameState, int x, int speed, BufferedImage image) {
        super(gameState, EntityType.POWERUP);
        this.image = image;
        this.speed = speed;
        this.x = x;
        this.y = -140;  // TEMPORARY -> ADJUST TO ANY GIVEN HEIGHT
    }

    public abstract void spawn();

    /**
     * @param adder value to add to speed every tick
     * @param multiplier value to multiply speed by to add to y
     */
    protected void move(double adder, double multiplier) {
        if(y <= gameState.getHeight()) {
            speed += adder;
            y += multiplier*speed;
        } else {
            remove();
        }
    }

}
