package de.duc.moorhuhn.entities.menucomponents;

import de.duc.moorhuhn.entities.Entity;
import de.duc.moorhuhn.entities.EntityType;

/**
 * Created by Corvin on 31.10.2016.
 */
public abstract class MenuComponent extends Entity {

    protected String id;

    public MenuComponent(EntityType type) {
        super(type);
    }
    public MenuComponent(EntityType type, int x, int y) {
        super(type, x, y);
    }
    public MenuComponent(EntityType type, int x, int y, int width, int height) {
        super(type, x, y, width, height);
    }

    public abstract void mouseEnter();
    public abstract void mouseLeave();

    public int getHitboxX() { return x-width/2; }
    public int getHitboxY() { return y-height/2; }
    public int getHitboxWidth() { return getHitboxX()+width; }
    public int getHitboxHeight() { return getHitboxY()+height; }

    public String getId() { return id; }
    public void setId(String s) { id = s; }
}
