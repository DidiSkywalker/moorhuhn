package de.duc.moorhuhn.entities.menucomponents;

import de.duc.moorhuhn.entities.EntityType;

import java.awt.*;

/**
 * Created by Corvin on 01.11.2016.
 */
public class Title extends MenuComponent {

    private String text;
    private int size, t;

    public Title(String text, int x, int y) {
        super(EntityType.TITLE, x, y);
        this.text = text;
        t = 0;
    }

    @Override
    public void mouseEnter() {

    }

    @Override
    public void mouseLeave() {

    }

    @Override
    public void update() {
        // a sin(b (x+c))+d
        size = (int)(50*Math.sin(t*0.045)+175);
        t++;
    }

    @Override
    public void draw(Graphics g) {
        //g.setFont(new Font("Copperplate Gothic Light", 0, size));
        g.setColor(new Color(250, 34, 10));
        g.setFont(new Font("Cooper Black", 0, size));
        int stringWidth = g.getFontMetrics().stringWidth(text);
        g.drawString(text, x-stringWidth/2, y);
    }

    @Override
    public void hit(Point p) {

    }
}
