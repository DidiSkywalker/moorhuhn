package de.duc.moorhuhn.entities.menucomponents.button;

import de.duc.moorhuhn.entities.EntityType;
import de.duc.moorhuhn.gfx.Asset;
import de.duc.moorhuhn.util.Debug;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Corvin on 31.10.2016.
 */
public class Button extends de.duc.moorhuhn.entities.menucomponents.MenuComponent {

    private String text;
    private Color color;
    private BufferedImage texture;
    private Set<ButtonListener> listeners;

    public Button(int x, int y, Color color) {
        super(EntityType.BUTTON, x, y, Asset.button.getWidth(), Asset.button.getHeight());
        listeners = new HashSet<>();
        texture = Asset.button;
        this.color = color == null ? new Color(250, 34, 10):color;
    }

    @Override
    public void mouseEnter() {
        texture = Asset.button_down;
        Debug.send("Button: MouseEnter");
    }

    @Override
    public void mouseLeave() {
        texture = Asset.button;
        Debug.send("Button: MouseLeave");
    }

    @Override
    public void update() {

    }

    @Override
    public void draw(Graphics g) {
        g.drawImage(texture, (x-width/2), (y-height/2), null);
        g.setColor(color);
        g.setFont(new Font("Copperplate Gothic Light", 0, 50));
        //g.setFont(new Font("Cooper Black", 0, 50));
        int stringWidth = g.getFontMetrics().stringWidth(text);
        g.drawString(text, x-stringWidth/2, y+15);
    }

    @Override
    public void hit(Point p) {
        Debug.send("Button hit!");
        for(ButtonListener l : listeners) {
            l.click();
            l.click(p);
        }
    }

    public String getText() { return text; }
    public Color getColor() { return color; }
    public void setText(String s) { this.text = s; }
    public void setColor(Color c) { color = c; }
    public void addButtonListener(ButtonListener l) { listeners.add(l); }
}
