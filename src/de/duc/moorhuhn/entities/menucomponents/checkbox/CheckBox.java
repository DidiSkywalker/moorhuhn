package de.duc.moorhuhn.entities.menucomponents.checkbox;

import de.duc.moorhuhn.entities.EntityType;
import de.duc.moorhuhn.gfx.Asset;
import de.duc.moorhuhn.util.Utils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Corvin on 01.11.2016.
 */
public class CheckBox extends de.duc.moorhuhn.entities.menucomponents.MenuComponent {

    private String text;
    private boolean checked;

    private CheckBoxGroup group;
    private Set<CheckBoxListener> listeners;

    private int stringWidth;

    private BufferedImage boxImage;
    private BufferedImage image;
    private BufferedImage image_down;
    private boolean selected;

    public CheckBox(int x, int y, BufferedImage boxImage) {
        super(EntityType.CHECKBOX, x, y);
        this.boxImage = boxImage;
        listeners = new HashSet<>();
        text = "";
        stringWidth = 0;
        height = 70;
        resize();
    }

    @Override
    public void mouseEnter() {
        selected = true;
    }

    @Override
    public void mouseLeave() {
        selected = false;
    }

    @Override
    public void update() {
        resize();
    }
    @Override
    public void draw(Graphics g) {
        g.setFont(new Font("Arial", 0, 50));
        stringWidth = g.getFontMetrics().stringWidth(text);
        g.drawImage(selected ? image_down:image, x-width/2, y-height/2, null);
        int x1 =x+width/2-80, y1 = y-25;
        if(boxImage == null) {
            g.setColor(Color.white);
            g.fillRect(x1, y1, 50, 50);
        } else {
            g.drawImage(boxImage, x1, y1, null);
        }
        if(checked) {
            g.setColor(Color.green);
            g.drawRect(x1, y1, 50, 50);
            // TODO: ADD TICKED TEXTURE
        }
        g.setColor(Color.black);
        g.drawString(text, x-width/2+30, y+height/2-17);
    }

    @Override
    public void hit(Point p) {
        toggle(!checked);
    }

    public void toggle(boolean b) {
        checked = b;

        if(group != null) group.tick(this, b);
        for(CheckBoxListener l : listeners) l.toggle(this, b);
    }

    private void resize() {
        width = stringWidth+120-((text == null || text.isEmpty()) ? 5:0);
        image = Utils.resize(Asset.button, stringWidth+120, 70);
        image_down = Utils.resize(Asset.button_down, stringWidth+120, 70);
        if(boxImage != null) boxImage = Utils.resize(boxImage, 50, 50);
    }

    public String getText() { return text; }
    public boolean isChecked() { return checked; }
    public void setChecked(boolean b) { checked = b; }
    public void setText(String s) { text = s; }
    public void setGroup(CheckBoxGroup group) { this.group = group; }
    public void addListener(CheckBoxListener l) { listeners.add(l); }
    public void removeListener(CheckBoxListener l) { listeners.remove(l); }
}
