package de.duc.moorhuhn.entities.menucomponents.checkbox;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Corvin on 01.11.2016.
 */
public class CheckBoxGroup {

    private String name;
    private CheckBox currentlyChecked;
    private Set<CheckBox> boxes;

    public CheckBoxGroup() {
        boxes = new HashSet<>();
    }

    public void tick(CheckBox box, boolean b) {
        currentlyChecked = box;
        for(CheckBox box1 : boxes)  if(!box1.equals(box)) box1.setChecked(!b);
    }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public void addBox(CheckBox box) { boxes.add(box); box.setGroup(this);  }
    public void removeBox(CheckBox box) { boxes.remove(box); }
    public void setCurrentlyChecked(CheckBox box) { currentlyChecked = box; }

}
