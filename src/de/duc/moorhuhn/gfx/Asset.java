package de.duc.moorhuhn.gfx;

import de.duc.moorhuhn.sound.SoundPlayer;
import de.duc.moorhuhn.util.Utils;

import java.awt.image.BufferedImage;
import java.net.URL;

/**
 * Created by Corvin on 25.10.2016.
 */
public class Asset {

    public static BufferedImage background;
    public static BufferedImage background_2;
    public static BufferedImage background_overlay;

    public static BufferedImage moorhuhn_1;
    public static BufferedImage moorhuhn_2;
    public static BufferedImage moorhuhn_3;
    public static BufferedImage moorhuhn_dead;

    public static BufferedImage heart;
    public static BufferedImage heart_damaged;

    public static BufferedImage gauge12_shell;
    public static BufferedImage m16_round;

    public static BufferedImage crosshair;
    public static BufferedImage hitmarker;
    public static BufferedImage cursor;
    public static BufferedImage button;
    public static BufferedImage button_down;

    public static BufferedImage lootCrate_health;

    public static URL shotgun_fire;
    public static URL shotgun_reload;

    public static URL m16_fire;

    public static URL theme;

    public static BufferedImage de, en;

    public Asset() {
        background = ImageLoader.loadImage("/textures/background_2.png");
        background_2 = ImageLoader.loadImage("/textures/background_2.png");
        background_overlay = ImageLoader.loadImage("/textures/background_overlay.png");

        moorhuhn_1 = ImageLoader.loadImage("/textures/moorhuhn_1.png");
        moorhuhn_2 = ImageLoader.loadImage("/textures/moorhuhn_2.png");
        moorhuhn_3 = ImageLoader.loadImage("/textures/moorhuhn_3.png");
        moorhuhn_dead = ImageLoader.loadImage("/textures/moorhuhn_dead.png");

        heart = ImageLoader.loadImage("/textures/heart.png");
        heart_damaged = ImageLoader.loadImage("/textures/heart_damaged.png");

        gauge12_shell = ImageLoader.loadImage("/textures/g12.png");
        m16_round = ImageLoader.loadImage("/textures/m16_round.png");

        crosshair = ImageLoader.loadImage("/textures/crosshair.png");
        hitmarker = ImageLoader.loadImage("/textures/hitmarker.png");
        cursor = ImageLoader.loadImage("/textures/cursor.png");
        button = Utils.resize(ImageLoader.loadImage("/textures/button.png"), 450, 100);
        button_down = Utils.resize(ImageLoader.loadImage("/textures/button_down.png"), 450, 100);

        lootCrate_health = ImageLoader.loadImage("/textures/lootCrate_health.png");

        shotgun_fire = SoundPlayer.class.getResource("/sounds/shotgun_fire.wav");
        shotgun_reload = SoundPlayer.class.getResource("/sounds/shotgun_reload.wav");

        m16_fire = SoundPlayer.class.getResource("/sounds/m16_fire.wav");

        theme = SoundPlayer.class.getResource("/sounds/narcos.wav");

        de = ImageLoader.loadImage("/textures/de.png");
        en = ImageLoader.loadImage("/textures/en.png");
    }

}
