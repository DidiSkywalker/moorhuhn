package de.duc.moorhuhn.input;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.core.states.StateManager;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Corvin on 31.10.2016.
 */
public class KeyManager implements KeyListener {

    private Game game;
    private StateManager stateManager;

    public KeyManager(Game game) {
        this.game = game;
        this.stateManager = game.getStateManager();
    }


    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        stateManager.getState().notify("Key pressed: "+e.getKeyCode());
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if(game.isInGame()) {
                stateManager.getState().setActive(false);
                stateManager.setState("menu");
                stateManager.getState().init();
            } else System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
