package de.duc.moorhuhn.input;

import de.duc.moorhuhn.core.Game;
import de.duc.moorhuhn.core.states.StateManager;
import de.duc.moorhuhn.util.Debug;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by Corvin on 31.10.2016.
 */
public class MouseManager implements MouseListener, MouseMotionListener {

    private Game game;
    private StateManager stateManager;

    private Point mouseLocation;
    private boolean mousePressed;

    public MouseManager(Game game) {
        this.game = game;
        this.stateManager = game.getStateManager();
        mouseLocation = new Point(0, 0);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(e.getButton() == 1) {
            mousePressed = true;
            stateManager.getState().leftClick(e.getPoint());
        } else if(e.getButton() == 3) {
            stateManager.getState().rightClick(e.getPoint());
        }
        Debug.send("Mouse clicked: "+e.getButton());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getButton() == 1) mousePressed = false;
    }

    public void mouseEntered(MouseEvent e) {  }
    public void mouseExited(MouseEvent e) { }
    public void mouseClicked(MouseEvent e) { }

    @Override
    public void mouseDragged(MouseEvent e) {
        mouseLocation = e.getPoint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        mouseLocation = e.getPoint();
    }

    public Point getMouseLocation() { return mouseLocation; }
    public boolean isMousePressed() { return mousePressed; }
}
