package de.duc.moorhuhn.lang;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Corvin on 01.11.2016.
 */
public class MessageManager {

    private String lang;

    private Map<String, String> messages;

    public MessageManager(String lang) {
        this.lang = lang;

        messages = new HashMap<>();

        readMessages("de");
        readMessages("en");
    }

    public String get(String key) {
        return messages.get(lang+"-"+key);
    }

    public String getLang() { return lang; }
    public void setLang(String l) { lang = l; }

    private void readMessages(String lang) {
        File file = new File(this.getClass().getResource("/lang/"+lang+".txt").getFile());
        List<String> msgs = new ArrayList<>();

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                msgs.add(line);
            }

            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        messages.put(lang+"-startButton", msgs.get(0));
        messages.put(lang+"-settingsButton", msgs.get(1));
        messages.put(lang+"-exitButton", msgs.get(2));
        messages.put(lang+"-devMode", msgs.get(3));
        messages.put(lang+"-points", msgs.get(4));
        messages.put(lang+"-wave", msgs.get(5));
        messages.put(lang+"-kills", msgs.get(6));
        messages.put(lang+"-shots", msgs.get(7));
        messages.put(lang+"-ammo", msgs.get(8));
        messages.put(lang+"-gameover", msgs.get(9));
        messages.put(lang+"-retry", msgs.get(10));
        messages.put(lang+"-backButton", msgs.get(11));
    }

}
