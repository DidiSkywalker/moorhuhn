package de.duc.moorhuhn.sound;

import de.duc.moorhuhn.gfx.Asset;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Corvin on 25.10.2016.
 * Credit: Dac Saunders (stackoverflow)
 */
public class SoundPlayer {

    public static void playSound(URL sound, float minusDb) {
        try {
            // Open an audio input stream.
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(sound);
            // Get a sound clip resource.
            Clip clip = AudioSystem.getClip();
            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
                // Credit: markusk (stackoverflow)
                FloatControl gainControl =
                        (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
                gainControl.setValue(-minusDb); // Reduce volume by 10 decibels.
                // -------------------------------
            clip.start();
            if(sound == Asset.theme) clip.loop(100);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

}
