package de.duc.moorhuhn.util;

import de.duc.moorhuhn.core.Game;

/**
 * Created by Corvin on 31.10.2016.
 */
public class Debug {

    public static void send(String msg) { if(Game.isDevMode()) System.out.println("[DEBUG] "+msg); }

}
