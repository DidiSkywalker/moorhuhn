package de.duc.moorhuhn.util;

import java.awt.*;

/**
 * Created by Corvin on 01.11.2016.
 */
public class MathUtils {

    public static Point randomRadiusPoint(Point center, int radius) {
        double x = Math.random() * (radius * 2) - radius;
        double y = Math.random() * (radius * 2) - radius;
        return new Point((int)(center.getX()+x), (int)(center.getY()+y));
    }

}
