package de.duc.moorhuhn.util;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Corvin on 31.10.2016.
 */
public class Utils {

    // Credit: Ocracoke (stackoverflow)
    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

}
