package de.duc.moorhuhn.weapons;

import de.duc.moorhuhn.entities.gameentities.GameEntity;
import de.duc.moorhuhn.gfx.Asset;
import de.duc.moorhuhn.sound.SoundPlayer;

import java.awt.*;

/**
 * Created by Corvin on 01.11.2016.
 */
public class M16 extends Weapon {

    private boolean reloading = false;
    private long reloadStart;

    public M16() {
        super("M16", 30, 0, 50, Asset.m16_round, Asset.m16_fire, Asset.shotgun_fire);
        automatic = true;
    }

    @Override
    public void update() {
        if(reloading && reloadStart+reloadTime >= System.currentTimeMillis()) reload(!(currentMag > 0));
    }

    @Override
    public int fire(Point p, GameEntity e) {
        if(canShoot()) {
            if (currentMag <= 0) {
                reload(true);
                return 0;
            }
            SoundPlayer.playSound(fireSound, 50);
            if (e != null) e.hit(p);
            currentMag--;
            lastShot = System.currentTimeMillis();
            return 1;
        }
        return 0;
    }

    @Override
    public void reload(boolean full) {
        if(reloading && reloadStart+reloadTime <= System.currentTimeMillis()) {
            SoundPlayer.playSound(Asset.shotgun_reload, 30);
            currentMag = magSize+(full ? 0:1);
            reloading = false;
        } else {
            if(!reloading)  {
                reloading = true;
                reloadStart = System.currentTimeMillis();
            }
        }
    }
}
