package de.duc.moorhuhn.weapons;

import de.duc.moorhuhn.entities.gameentities.GameEntity;
import de.duc.moorhuhn.gfx.Asset;
import de.duc.moorhuhn.sound.SoundPlayer;
import de.duc.moorhuhn.util.MathUtils;

import java.awt.*;

/**
 * Created by Corvin on 01.11.2016.
 */
public class Shotgun extends Weapon {
    public Shotgun() {
        super("Shotgun", 8, 0, 300, Asset.gauge12_shell, Asset.shotgun_fire, Asset.shotgun_fire);
    }

    @Override
    public void update() { }

    @Override
    public int fire(Point p, GameEntity e) {
        if(canShoot()) {
            if(currentMag <= 0) {
                reload(true);
                return 0;
            }
            SoundPlayer.playSound(fireSound, 20);
            for(int i = 0; i < 12; i++) {
                Point rPoint = MathUtils.randomRadiusPoint(p, 50);
                if(e != null) e.hit(rPoint);
            }
            currentMag--;
            SoundPlayer.playSound(reloadSound, 30);
            lastShot = System.currentTimeMillis();
            return 1;
        }
        return 0;
    }

    @Override
    public void reload(boolean full) {
        currentMag = magSize;
    }
}
