package de.duc.moorhuhn.weapons;

import de.duc.moorhuhn.entities.gameentities.GameEntity;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;

/**
 * Created by Corvin on 01.11.2016.
 */
public abstract class Weapon {

    protected String name;

    // Magazine
    protected final int magSize;
    protected int currentMag;

    // Reloading
    protected int reloadTime, shotDelay;

    protected boolean automatic;

    protected long lastShot;

    // TODO: Implement weapon image
    protected BufferedImage ammoIcon;
    protected URL fireSound;
    protected URL reloadSound;

    public Weapon(String name, int magSize, int reloadTime, int shotDelay, BufferedImage ammoIcon, URL fireSound, URL reloadSound) {
        this.name = name;
        this.magSize = magSize;
        this.reloadTime = reloadTime;
        this.shotDelay = shotDelay;
        this.ammoIcon = ammoIcon;
        this.fireSound = fireSound;
        this.reloadSound = reloadSound;
        automatic = false;
        currentMag = magSize;
    }

    public abstract void update();
    public abstract int fire(Point p, GameEntity e);
    public abstract void reload(boolean full);

    public boolean canShoot() {
        long currentTime = System.currentTimeMillis();
        long neededTime = lastShot+shotDelay;
        long difference = currentTime-neededTime;
        return  lastShot == 0 || difference >= 0;
    }


    public String getName() { return name; }
    public int getMagSize() { return magSize; }
    public int getCurrentMag() { return currentMag; }
    public int getReloadTime() { return reloadTime; }
    public int getShotDelay() { return shotDelay; }
    public long getLastShot() { return lastShot; }
    public boolean isAutomatic() { return automatic; }
    public BufferedImage getAmmoIcon() { return ammoIcon; }
    public URL getFireSound() { return fireSound; }
    public URL getReloadSound() { return reloadSound; }
}
